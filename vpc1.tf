
  resource "aws_vpc" "tfvpc" {
    cidr_block       = "190.160.0.0/16"
    instance_tenancy = "default"
  
    tags = {
      Name = "demovpc"
  }
}

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.tfvpc.id
  cidr_block = "190.160.2.0/24"

  tags = {
    Name = "public_subnet"
  }
}

resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.tfvpc.id
  cidr_block = "190.160.1.0/24"

  tags = {
    Name = "private_subnet"
  }
}


  resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.tfvpc.id
  
    tags = {
      Name = "internet"
  }
}


  resource "aws_eip" "nat_eip" {
    depends_on = [aws_internet_gateway.igw
  ]
    vpc      = true
  tags = {
    Name = "nat_gate_eip"
  }
}
resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public.id

  tags = {
    Name = "nat_gateway"
  }
}

resource "aws_network_acl" "My_VPC_Security_ACL" {
  vpc_id = aws_vpc.tfvpc.id
  subnet_ids = [ aws_subnet.public.id
  ]

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }
  
  
  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  }
  
   
  ingress {
    protocol   = "tcp"
    rule_no    = 300
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }
  
  
  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  
   
  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0  
    to_port    = 0
  }
 
  
  egress {
    protocol   = "tcp"
    rule_no    = 300
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
tags = {
    Name = "My VPC ACL"
  }
}

resource "aws_route_table" "publicroute" {
  vpc_id = aws_vpc.tfvpc.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.igw.id
  }

    tags = {
        Name = "public"
  }
}
  resource "aws_route_table" "privateroute" {
    vpc_id = aws_vpc.tfvpc.id
  
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
  }

      tags = {
          Name = "private"
  }
}


    resource "aws_route_table_association" "pubrouteassociation" {
      subnet_id      = aws_subnet.public.id
      route_table_id = aws_route_table.publicroute.id
}
    resource "aws_route_table_association" "prirouteassociation" {
      subnet_id      = aws_subnet.private.id
      route_table_id = aws_route_table.privateroute.id
}

    resource  "aws_security_group" "sg"  {
      name        = "first_sg"
      description = "Allow TLS inbound traffic"
      vpc_id      = aws_vpc.tfvpc.id
      ingress = [
    {
      "cidr_blocks": [
        "0.0.0.0/0"
      ]
              "description": "HTTPS"
              "from_port": 443
              "ipv6_cidr_blocks": null
              "prefix_list_ids": null
              "protocol": "tcp"
              "security_groups": null
              "self": null
              "to_port": 443
    }
  ]
   
       egress = [
    {
      "cidr_blocks": [
        "0.0.0.0/0"
      ]
              "description": "HTTPs"
              "from_port": 0
              "ipv6_cidr_blocks": null
              "prefix_list_ids": null
              "protocol": "tcp"
              "security_groups": null
              "self": null
              "to_port": 65535
    }
  ]
      tags = {
          Name = "first_sg"
  }
}