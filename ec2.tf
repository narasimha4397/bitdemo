            resource "aws_instance" "tf_instance" {
                ami           = "ami-083ac7c7ecf9bb9b0"
                instance_type = "t2.micro"
                subnet_id = "subnet-031986ec9b0297fb6"
                security_groups = [
                "sg-0b179a70cea1f7421"
              ]
                key_name = "aws_key_pair.key.id"
              
                tags = {
                  Name = "webapp_instance"
              }
            }
              resource "aws_key_pair" "key" {
                key_name   = "sample"
                public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCykXhPYekHorOHfxlVaS0kenEUeALGyXf/SIa/B8qLN7Wst+VRCVR8/9xpqcJqLInK/Zu6R1g+eV6MrlhEOQIaaZW2vMdw8u0aa3OfVbSxqN3SpFRBSS5pYWk0GSdIAdxpRU8UTAIVeWr+iKJ7iiFJjBry3Yh7saHVkR1svKmR7EGTNJqXt5TDJuOgeMDaxfziHIeE9F2Kt8KHMRq4czLBA3fnfJB2TJc1N7bvNBgaKNCb+j/8ekOjZD6KisYDqN5ZIURHj+U/4IOcLqoya0S+G46R9jsrk8Crnp1JAKKcrqjUyXkHPpC2VBUQaiesGaKWd8mR6Vxvrj+2WbbU5HT32Qx1AeF1347jCMjLROdRGnIRzuINoppDAJPBhYNXyiPVvioSzUTVlkx6se/wKLg4e588ErDS/danZ7910KIZhbrBrN39mVXiJnD/uNYxsIDckN8cacEeY9PQOlNR7ogYSRgYjBgZ1XRZh4mNKmRKmLxbfnVY6ym+eMzM1pkUDis= bharathi reddy@DESKTOP-3F7KUE0"
            }